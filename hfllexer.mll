{
  open Hflparser
}

let white   = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let num   = ['0'-'9']
let lchar = ['a'-'z']
let uchar = ['A'-'Z']

rule token = parse
  | white                     { token lexbuf }
  | newline                   { incr line; token lexbuf }
  | ';'                       { comment lexbuf }

  | "ff"                      { TFalse }
  | "tt"                      { TTrue  }

  | "Pr"                      { TPrType }

  | "0"                       { TZero }
  | "+"                       { TPlus }
  | "-"                       { TMinu }

  | "/\\"                     { TAnd }
  | "\\/"                     { TOrr }

  | "_L"                      { TLambda }
  | "->"                      { TArrow  }

  | ":"                       { TSemiColon }

  | "."                       { TDot }
  | "_v"                      { TGfp }
  | "_u"                      { TLfp }

  | "<"                       { TLeftABrack  }
  | ">"                       { TRightABrack }
  | "["                       { TLeftSBrack  }
  | "]"                       { TRightSBrack }

  | "("                       { TLeftParen  }
  | ")"                       { TRightParen }

  | ","                       { TComma }
  | "="                       { TEq }

  | "%GRAMMAR"                { TGRAMMAR }
  | "%TRANSITION"             { TTRANSITION }

  | uchar (uchar|lchar|num)* as lxm { TUIdent lxm }
  | lchar (uchar|lchar|num)* as lxm { TLIdent lxm }

  | _                         { raise (Failure  
                                            ("unrecognized token: " ^  
                                            (Lexing.lexeme lexbuf)))}
  | eof                       { TEOF }

  and comment = parse
  | newline { incr line; token lexbuf }
  |  _      { comment lexbuf }
