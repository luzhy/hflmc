.DEFAULT_GOAL = all
.PHONY: all test qtest clean

SOURCE = x.ml log.ml profile.ml syntax.ml hflparser.ml hfllexer.ml base.ml flow.ml saturation.ml parityGame.ml hflconfig.ml  modelCheck.ml tests.ml
SOURCE2 = x.ml log.ml profile.ml syntax.ml hflparser.ml hfllexer.ml base.ml flow.ml saturation.ml parityGame.ml hflconfig.ml modelCheck.ml tests.ml
LIBS = batteries,cmdliner,pa_comprehension

all: hflmc.opt hflmctop hflmc

hfllexer.ml: hfllexer.mll
	ocamllex hfllexer.mll

hflparser.ml: hflparser.mly
	ocamlyacc hflparser.mly
	$(RM) hflparser.mli

hflmc: $(SOURCE2) hflmc.ml
	ocamlfind ocamlc   -syntax camlp4o -package $(LIBS) -g -linkpkg -o hflmc $(SOURCE2) hflmc.ml 

hflmctop: $(SOURCE2) hflmctop.ml
	ocamlfind ocamlmktop -syntax camlp4o -package $(LIBS),utop -g -thread -linkpkg -o hflmctop  $(SOURCE2) hflmctop.ml 

hflmc.opt: $(SOURCE) hflmc.ml
	ocamlfind ocamlopt -syntax camlp4o -package $(LIBS) -g -p -linkpkg -o hflmc.opt $(SOURCE) hflmc.ml

xml_productivity: $(SOURCE) productivity_main.ml
	ocamlfind ocamlopt -syntax camlp4o -package $(LIBS) -g -p -linkpkg -o productivity.opt $(SOURCE) productivity_main.ml

# just for me
test: hflmc hflmc.opt
	ruby test.rb

qtest:
	mkdir -p qtest
	qtest -o qtest/all_tests.ml --preamble-file qtest/qtest_preamble.ml extract $(SOURCE)
	ocamlfind ocamlc -syntax camlp4o -package batteries,pa_comprehension,oUnit,QTest2Lib -linkpkg -o qtest/all_tests $(SOURCE) qtest/all_tests.ml
	./qtest/all_tests

clean:
	$(RM) gmon.out *.annot *.automaton *.conflicts hflparser.ml hfllexer.ml test hflmc hflmctop *.opt top qtest.targets.log *.cmo *.cmx *.o *.cmi *.mli hflmc.zip
	$(RM) qtest/all_tests*
tar:
	zip -r hflmc.zip Makefile make.conf *.ml hflparser.mly hfllexer.mll test.rb test_answers.txt inputs

mini: syntax.ml hflparser.ml hfllexer.ml
	ocamlc syntax.ml

