open Batteries
open Syntax
open X

type state = string
type symbol = string

module Term = struct

  type term_type = 
    | TermTTT
    | TermFFF
    | TermBox
    | TermDmd
    | TermAnd
    | TermOrr
    | NonTerm 
    | Variabl

  type t = Term of term_type * symbol * t list

  let modify_children f (Term (term_type, symbol, children)) =
    Term (term_type, symbol, f children)

  let rec to_string (Term (term_type, symbol, children)) =
    let children_str = String.join " " (List.map to_string children) in
    let parent_str =  
      match term_type with
      | TermTTT -> "TT"
      | TermFFF -> "FF"
      | TermBox -> "[" ^ symbol ^ "]"
      | TermDmd -> "<" ^ symbol ^ ">"
      | TermAnd -> "/\\"
      | TermOrr -> "\\/"
      | NonTerm -> symbol
      | Variabl -> symbol
    in
    "(" ^ parent_str ^ " " ^ children_str ^ ")"

end

module Sort = struct

  type id =
    | IVar of (symbol * symbol)
    | ITmp of int

  let id_to_string = function
    | IVar (f, x) -> f ^ "$" ^ x
    | ITmp i -> string_of_int i

  type t =
    | B
    | F of t * t
    | A of id

  let fromList lst typ =
    List.fold_right (fun t typ -> F (t, typ)) lst typ

  let rec to_string = function
    | B          -> "o"
    | F (t1, t2) -> "(" ^ to_string t1 ^ " -> " ^ to_string t2 ^ ")"
    | A id       -> "[" ^ id_to_string id ^ "]"

  let rec order = function
    | B -> 0
    | F (t1, t2) -> max (order t1 + 1) (order t2)
    | A id -> 0 (* invalid_arg "The order is undefined." *)

end


module Hes = struct

  type t = Hes of (symbol, (symbol list * Term.t)) Map.t * 
                  (symbol, Sort.t) Map.t * 
                  symbol * 
                  state

  let to_string (Hes (rules, sorts, _, _)) =
    let rulesStr =
      String.join
        "\n"
        (List.map
           (fun (symbol, (args, term)) -> symbol ^ " " ^ String.join " " args ^ " -> " ^ Term.to_string term ^ ".")
           (Map.bindings rules))
    in
    let sortsStr =
      String.join
        "\n"
        (List.map
           (fun (symbol, sort) -> symbol ^ " :: " ^ Sort.to_string sort)
           (Map.bindings sorts))
    in
    rulesStr ^ "\n\n" ^ sortsStr

  let getArg (Hes (rewritingRules, _, _, _)) f i =
    (f, List.nth (fst (Map.find f rewritingRules)) i)

  let getBody (Hes (rewritingRules, _, _, _)) f =
    snd (Map.find f rewritingRules)

  let get (Hes (rules, _, _, _)) f =
    Map.find f rules

  let order (Hes (_, sorts, _, _)) =
    reduce max (map Sort.order (Map.values sorts))

  let modifyRule f (Hes (rules, sorts, symbol, state)) =
    let rules' = f rules in
    Hes (rules', sorts, symbol, state)

  let keys (Hes (rules, _, _, _)) =
    Set.of_enum (Map.keys rules)

  let variables (Hes (rules, _, _, _)) =
    Set.of_list (List.flatten (List.map fst (List.of_enum (Map.values rules))))

end

module Lts = struct

  type t = Lts of (state * symbol, state Set.t) Map.t * state Set.t * symbol Set.t

  let to_string (Lts (delta, states, acts)) =
    let string_of_delta delta =
      List.fold_right 
        (fun ((a,b), c) d ->
           d ^ 
           "    ( " ^ a ^ ", " ^ b ^ " ) -> " ^
           "[ " ^ String.join "," (Set.elements c) ^ " ] \n")
        (Map.bindings delta)
        ""

    in
    String.join "\n" [
      "{";
      "  delta  = "; string_of_delta delta;
      "  states = "; "[ " ^ String.join ", " (Set.elements states) ^ " ]";
      "  acts   = "; "[ " ^ String.join ", " (Set.elements acts) ^ " ]";
      "}"
    ]

end

open Term

let rec allNonTerminals (Term (termType, symbol, children)) =
  let rest = unions (List.map allNonTerminals children)
  in
  match termType with
  | NonTerm -> Set.add symbol rest
  | _ -> rest

let dependencyGraph rules =
  Map.mapi
    (fun symbol (_, body) ->
      Log.printf 2 "%s: \n" symbol;
      let ans = allNonTerminals body
      in
      Set.iter (Log.println 2) ans;
      Log.println 2 "";
      ans
    )
    rules

let all_terminals rules =
  let rec search (Term.Term (term_type, symbol, children)) =
    let st =
      match term_type with
      | TermTTT
      | TermFFF 
      | TermBox 
      | TermDmd 
      | TermAnd 
      | TermOrr -> Map.singleton symbol term_type
      | NonTerm 
      | Variabl -> Map.empty
    in
    List.reduce Map.union (st :: (List.map search children))
  in
  reduce Map.union (map search (map snd (Map.values rules)))


module SortCheck = struct

  open Term
  open Sort

  let _cnt = ref 0

  let newType () =
    let ret = A (ITmp (!_cnt))
    in
    incr _cnt;
    ret

  let rec genConstraintsTerm rules env curNT (Term (termType, symbol, children)) =
    let types, constraints = 
      List.split (List.map (genConstraintsTerm rules env curNT) children)
    in
    let n = 
      List.length children
    in
    match termType with
    | TermTTT
    | TermFFF 
    | TermBox 
    | TermDmd 
    | TermAnd 
    | TermOrr ->
      let retTyp = newType () in
      let constraints = 
        Set.add 
          (Map.find symbol env, Sort.fromList (List.make n B) retTyp) 
          (Set.union 
             (Set.of_list
                (List.map (fun typ -> (typ, B)) types))
             (unions constraints))
      in
      (retTyp, constraints)

    | NonTerm ->
      let (args, _) = Map.find symbol rules in
      let retTyp = 
        fromList
          (List.map 
             (fun arg -> A (IVar (symbol, arg))) 
             (List.drop n args))
          B
      in
      (retTyp, 
       Set.union 
         (Set.of_list 
            (List.map 
               (fun (arg, typ) -> (A (IVar (symbol, arg)), typ))
               (List.combine (List.take n args) types))) 
         (unions constraints))

    | Variabl ->
      let retTyp = newType () in
      (retTyp, 
       Set.add 
         (A (IVar (curNT, symbol)), fromList types retTyp)
         (unions constraints))

  let genConstraintsRule rules env (symbol, (_, body)) =
    let (retTyp, constraints) = 
      (genConstraintsTerm rules env symbol body)
    in
    Set.add (retTyp, B) constraints

  let genConstraints rules env =
    unions 
      (List.map (genConstraintsRule rules env) (Map.bindings rules))

  exception UnificationError of string

  let rec occur id = function
    | B -> false
    | F (x, y) -> occur id x || occur id y
    | A id' -> id = id'

  let rec subst id kind = function
    | B -> B
    | F (x, y) -> F (subst id kind x, subst id kind y)
    | A id' ->
      if id = id' then
        kind
      else
        A id'

  let rec unify = function
    | []                             -> []
    | (B, B) :: xs                   -> unify xs
    | (F (x1, y1), F (x2, y2)) :: xs -> unify ((x1, x2) :: (y1, y2) :: xs)
    | (A id, kind) :: xs
    | (kind, A id) :: xs             ->
      if occur id kind then
        match kind with
        | A id' when id = id' -> unify xs
        | _ ->
          raise 
            (UnificationError 
               ("Dependency loop detected: " ^ 
                Sort.id_to_string id ^ 
                " occurs in " ^ 
                Sort.to_string kind))
      else
        (id, kind) :: 
        unify 
          (List.map 
             (fun (s, t) -> (subst id kind s, subst id kind t)) 
             xs)
    | (k1, k2) :: _                 ->
      raise 
        (UnificationError 
           ("Sorts are different: " ^ 
            Sort.to_string k1 ^ 
            " <> " ^ 
            Sort.to_string k2))

  let rec lookup mp kind =
    match mp with
    | []                 -> kind
    | (id', kind') :: xs ->
      lookup xs (subst id' kind' kind)

  let resolve mp rules constraints =
    let () = Log.println 4 "map constructed." in
    Map.mapi
      (fun f (args, _) ->
         fromList
           (List.map
              (fun x -> lookup mp (A (IVar (f, x))))
              args)
           B)
      rules

  let sortCheck rules =
    let ()          = Log.println 4 "Checking sorts..." in
    let terminals   = (Set.of_enum (Map.keys (all_terminals rules))) in
    let env         = Set.map_to (fun _ -> newType()) terminals in
    let constraints = genConstraints rules env in
    let ()          = Log.println 4 "Constraints enumerated." in
    let ()          = 
      Set.iter 
        (fun (t1, t2) -> 
           Log.println 
             4 
             (Sort.to_string t1 ^ " = " ^ Sort.to_string t2)) 
        constraints in
    let mp         = unify (Set.elements constraints) in
    (resolve mp rules constraints, Map.map (lookup mp) env)
end

let rec replaceT hesM =
  let rec rT (Term (termType, symbol, children)) =
    let children' = List.map rT children in
    match termType with
    | TermBox 
    | TermDmd
    | TermTTT
    | TermFFF
    | TermAnd
    | TermOrr -> Term (NonTerm, "$" ^ symbol, children')
    | _       -> Term (termType, symbol, children')
  in
  let rS (vl, tm) = (vl, rT tm) in
  Map.map rS hesM

let testfunc (rewriting_rules, transitions) =
  let rec retreveArgs form =
    match form with
    | Syntax.Lam(Syntax.HflV v, t, f) -> 
      let res = retreveArgs f in 
      (v :: fst res, snd res)
    | _ -> 
      ([], form)
  in
  let rec to_Tree_Rep hESForm =
    match hESForm with
    | Syntax.TTT   -> Term (TermTTT, "TT", [])
    | Syntax.FFF   -> Term (TermFFF, "FF", []) 
    | Syntax.Var x ->
      begin
        match x with
        | HflV str ->
          Term (Variabl, str, [])
        | HflN str -> 
          Term (NonTerm, str, [])
      end

    | Syntax.Box (actStr, f) -> 
      let child = to_Tree_Rep f in
      Term (TermBox, actStr, [child])

    | Syntax.Dmd (actStr, f) -> 
      let child = to_Tree_Rep f in
      Term (TermDmd, actStr, [child])

    | Syntax.And (f1, f2) -> 
      let child1 = to_Tree_Rep f1 in
      let child2 = to_Tree_Rep f2 in
      Term (TermAnd, "#And", [child1;child2])

    | Syntax.Orr (f1, f2) -> 
      let child1 = to_Tree_Rep f1 in
      let child2 = to_Tree_Rep f2 in
      Term (TermOrr, "#Orr", [child1;child2])

    | Syntax.App (f1, f2) -> 
      let child1 = to_Tree_Rep f1 in
      let child2 = to_Tree_Rep f2 in
      modify_children (fun s -> s @ [child2]) child1

    | _ -> Term (TermFFF, "Error", [])
  in
  List.fold_right
    (fun (x, vt, f) m ->
       let (vList, sF) = retreveArgs f in
       let tup = (vList, to_Tree_Rep sF) in
       (Map.add x tup m))
    rewriting_rules
    Map.empty

let to_Hes_and_Lts (rewriting_rules, transitions) =
  let rec retreveArgs form =
    match form with
    | Syntax.Lam(Syntax.HflV v, t, f) -> 
      let res = retreveArgs f in 
      (v :: fst res, snd res)
    | _ -> 
      ([], form)
  in
  let rec to_Tree_Rep hESForm =
    match hESForm with
    | Syntax.TTT   -> Term (TermTTT, "TT", [])
    | Syntax.FFF   -> Term (TermFFF, "FF", []) 
    | Syntax.Var x ->
      begin
        match x with
        | HflV str ->
          Term (Variabl, str, [])
        | HflN str -> 
          Term (NonTerm, str, [])
      end

    | Syntax.Box (actStr, f) -> 
      let child = to_Tree_Rep f in
      Term (TermBox, "#A_" ^ actStr, [child])

    | Syntax.Dmd (actStr, f) -> 
      let child = to_Tree_Rep f in
      Term (TermDmd, "#E_" ^ actStr, [child])

    | Syntax.And (f1, f2) -> 
      let child1 = to_Tree_Rep f1 in
      let child2 = to_Tree_Rep f2 in
      Term (TermAnd, "#And", [child1;child2])

    | Syntax.Orr (f1, f2) -> 
      let child1 = to_Tree_Rep f1 in
      let child2 = to_Tree_Rep f2 in
      Term (TermOrr, "#Orr", [child1;child2])

    | Syntax.App (f1, f2) -> 
      let child1 = to_Tree_Rep f1 in
      let child2 = to_Tree_Rep f2 in
      modify_children (fun s -> s @ [child2]) child1

    | _ -> Term (TermFFF, "Error", [])
  in
  let hesMap =
    List.fold_right
      (fun (x, vt, f) m ->
         let (vList, sF) = retreveArgs f in
         let tup = (vList, to_Tree_Rep sF) in
         (Map.add x tup m))
      rewriting_rules
      Map.empty
  in

  let hesForTerm =
    let termMap = all_terminals hesMap in
    Map.foldi
      (fun a b c ->
         match b with
         | TermFFF
         | TermTTT -> Map.add 
                        ("$" ^ a)
                        ([], 
                         Term(b, a, [])) 
                        c
         | TermBox 
         | TermDmd -> Map.add 
                        ("$" ^ a)
                        (["x"], 
                         Term(b, a, [Term(Variabl, "x", [])]))
                        c
         | TermAnd
         | TermOrr -> Map.add 
                        ("$" ^ a)
                        (["x"; "y"], 
                         Term(b, a, 
                              [Term(Variabl, "x", []);
                               Term(Variabl, "y", [])]))
                        c
         | _       -> c)
      termMap
      Map.empty
  in
  let hesInst =
    let newMap = Map.union (replaceT hesMap) hesForTerm in
    let sSy, sSt =
      (fst3 (List.hd rewriting_rules), fst3 (List.hd transitions))
    in
    Hes.Hes (newMap, 
             Map.empty, 
             sSy, 
             sSt)
  in
  let ltsInst =
    List.fold_right
      (fun (a,b,c) d ->
         let m = fst3 d in
         let s = snd3 d in
         let t = thd3 d in
         (Map.modifyDefault (Set.singleton c) (a,b) (Set.add c) m,
          Set.add c (Set.add a s),
          Set.add b t))
      transitions
      (Map.empty, Set.empty, Set.empty)
  in
  (hesInst, Lts.Lts (fst3 ltsInst, snd3 ltsInst, thd3 ltsInst))

module rec Atype :sig

  type t =
    | Q of state
    | A of Itype.t * Atype.t

  val take : int -> t -> Itype.t list

  val drop : int -> t -> Atype.t

  val init : t -> Itype.t list
  val last : t -> state

  val to_string : t -> string

end = struct

  type t =
    | Q of state
    | A of Itype.t * Atype.t

  let rec to_string = function
    | Q q -> q
    | A (x, y) -> "(" ^ Itype.to_string x ^ " -> " ^ 
                  to_string y ^ ")"

  let rec take' n atype =
    match n, atype with
    | 0, _ -> []
    | n, Atype.Q _ -> invalid_arg "n is too big."
    | n, Atype.A (x, y) -> x :: take' (n-1) y

  let take n atype =
    try take' n atype with _ -> invalid_argf "Atype.take %d %s" n (to_string atype)

  let rec drop' n atype =
    match n, atype with
    | 0, atype -> atype
    | n, Atype.Q q -> invalid_arg "n is too big."
    | n, Atype.A (_, y) -> drop' (n-1) y

  let drop n atype =
    try drop' n atype with _ -> invalid_argf "Atype.drop %d %s" n (to_string atype)

  let rec init = function
    | Q _ -> []
    | A (x, y) -> x :: init y

  let rec last = function
    | Atype.Q q -> q
    | Atype.A (_, y) -> last y

end

and Itype : sig

  type t = Atype.t SSet.t

  val to_string : t -> string

  val add : Atype.t -> t -> t
  val singleton : Atype.t -> t
  val union : t -> t -> t
  val empty : t

end = struct

  type t = Atype.t SSet.t

  let rec to_string t =
    if SSet.is_empty t then
      "T"
    else if SSet.cardinal t = 1 then
      Atype.to_string (SSet.choose t)
    else
      "(" ^ String.join " /\\ " (List.map Atype.to_string (SSet.elements t)) ^ ")"

  let add atym t =
    SSet.add atym t

  let singleton atym =
    SSet.singleton atym

  let union ity1 ity2 =
    SSet.union ity1 ity2

  let empty = SSet.empty

end

module TypeBinding = struct
  type t = symbol * Atype.t

  let to_string (symbol, typ) =
    symbol ^ " : " ^ Atype.to_string typ

end

module TypeEnv : sig
  type t = (symbol * Itype.t) Set.t
  val to_string : t -> string
end = struct
  type t = (symbol * Itype.t) Set.t

  let to_string env =
    "{ " ^ 
    String.join
      ", " 
      (List.map
         (fun (symbol, ity) -> 
            symbol ^ " : " ^ Itype.to_string ity) 
         (Set.elements env)) ^ 
    " }"
end
