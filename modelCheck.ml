open Batteries
open X
open Base
open Syntax

module Result = struct
  type t = Safe | Unsafe | Timeout | Error

  let to_string = function
    | Safe    -> "safe" | Unsafe  -> "unsafe"
    | Timeout -> "timeout"
    | Error   -> "error"

end

type ('a, 'b) loop =
  | Continue of 'a
  | Break of 'b

let rec for_ lst a g =
  match lst with
  | [] ->
    None
  | x :: xs ->
    begin
      match g x a with
      | Break y -> Some y
      | Continue na -> for_ xs na g
    end

open Result

let rec searchNT nt (Term.Term (termType, symbol, children)) =
  (match termType with
   | Term.NonTerm -> symbol = nt
   | _ -> false) || 
  List.exists (searchNT nt) children

let rec searchNT_flow nt (Flow.Flow (head, _)) =
  head = nt

let judgs_to_bindings judgs =
  unions
    (List.map
       (fun (f, ty_envs) ->
          Set.of_enum
            (map
               (fun (ty, _) -> (f, ty))
               (Map.enum ty_envs)))
       (Map.bindings judgs))

let forward_dependency hors flow =
  Set.map_to
    (fun f ->
       Set.filter 
         (fun g -> 
            let (args, body) = Hes.get hors g in
            List.exists
              (fun x -> 
                 Set.exists 
                   (searchNT_flow f)
                   (Map.findDefault Set.empty (g, x) flow))
              args
            || searchNT f body)
         (Hes.keys hors))
    (Hes.keys hors)

let merge_toprop toprop add_toprop =
  toprop @ List.filter (fun f -> not (List.mem f toprop)) add_toprop

let convert_env env =
  Set.of_list 
    (Set.elements (
        unions
          (List.map
             (fun (f, ity) -> 
                Set.of_list
                  (List.map 
                     (fun (aty, m) -> (f, (aty, m))) 
                     (Set.elements ity)))
             (Set.elements env))))

let convert_judgments judgments =
  unions
    (List.map
       (fun (f, ty_envs) ->
          unions
            (List.map
               (fun (ty, envs) -> 
                  Set.map
                    (fun env -> ((f, ty), env))
                    envs)
               (Map.bindings ty_envs)))
       (Map.bindings judgments))

let merge_map_of_set mp1 mp2 =
  Map.merge
    (fun key set1 set2 ->
       match (set1, set2) with
       | None, None -> assert false
       | Some set1, None -> Some set1
       | None, Some set2 -> Some set2
       | Some set1, Some set2 -> Some (Set.union set1 set2))
    mp1
    mp2

let add_judgments tj nt tj_new =
  Set.fold 
    (fun tj' (ty, env) -> 
       Map.modify 
         nt
         (fun ty_envs ->
            Map.modify ty (Set.add env) (if Map.mem ty ty_envs then ty_envs else Map.add ty Set.empty ty_envs))
         (if Map.mem nt tj' then tj' else Map.add nt Map.empty tj'))
    tj
    tj_new

let merge_map_of_sset mp1 mp2 =
  Map.merge
    (fun key set1 set2 ->
       match (set1, set2) with
       | None, None -> assert false
       | Some set1, None -> Some set1
       | None, Some set2 -> Some set2
       | Some set1, Some set2 -> Some (Set.union set1 set2))
    mp1
    mp2

let update_vartc flow2 g newtypeset vartc =
  Set.fold
    (fun vartc ((f,x), n) ->
       Map.modifyDefault
         Map.empty
         f
         (fun mp ->
            Set.fold
              (fun mp ty -> 
                 Map.modifyDefault
                   Map.empty
                   x
                   (Map.modifyDefault
                      SSet.empty
                      (Atype.drop n ty)
                      (SSet.add (Flow.Flow (g, n))))
                   mp)
              mp
              newtypeset)
         vartc)
    vartc
    (Map.findDefault Set.empty g flow2)

let create_vartc tj flow2 =
  Map.foldi
    (fun f ty_envs c ->
       update_vartc 
         flow2
         f 
         (Set.of_enum (Map.keys ty_envs)) 
         c)
    tj
    Map.empty

let number_of_judgments tj =
  Map.fold (fun typ_envs sum -> Map.fold (fun envs sum -> sum + Set.cardinal envs) typ_envs sum) tj 0

let print_tj tj =
  Map.iter
    (fun f aty_envs -> 
       Map.iter
         (fun aty envs ->
            Set.iter
              (fun env ->
                 prerr_endline 
                   ((Base.TypeEnv.to_string env) ^ " |- " ^
                    f ^ " : " ^
                    (Atype.to_string aty)))
              envs)
         aty_envs)
    tj

let print_judgments tj =
  if !Log.__level >= 3 then
    Map.iter
      (fun f aty_envs -> 
         Map.iter
           (fun aty envs ->
              Set.iter
                (fun env ->
                   Log.printf 3
                     "%s |- %s : %s\n" 
                     (TypeEnv.to_string env)
                     f
                     (Atype.to_string aty))
                envs)
           aty_envs)
      tj


let print_vartc v =
  Map.iter
    (fun a1 b1 ->
       prerr_endline a1;
       Map.iter 
         (fun a2 b2 ->
            prerr_endline ("- " ^ a2);
            Map.iter 
              (fun a3 b3 ->
                 prerr_endline ("-- " ^ (Atype.to_string a3));
                 SSet.iter
                   (fun (Flow.Flow (a4, b4)) ->
                      prerr_endline ("--- (" ^ a4 ^ ", " ^ (string_of_int b4) ^ " )"))
                   b3)
              b2)
         b1)
    v


let print_vartc_ vartc =
  Map.iter
    (fun f vtss ->
       Map.iter
         (fun x aty_flows -> 
            Log.printf 3 "%s.%s: \n" f x;
            Set.iter 
              (fun (aty, flows) -> 
                 Log.printf 3 "%s from " (Atype.to_string aty);
                 Set.iter (fun flow -> Log.printf 3 "%s " (Flow.to_string flow)) flows;
                 Log.println 3 ""
              )
              aty_flows
         )
         vtss)
    vartc

let judgs_to_map tj =
  Map.map (fun ty_envs -> Set.of_enum (Map.keys ty_envs)) tj

let print_nttc nttc =
  Map.iter
    (fun f tys ->
       Set.iter 
         (fun ty -> Log.printf 3 "%s : %s\n" f (Atype.to_string ty))
         tys
    )
    nttc

let print_stats tj nttc =
  Log.printf 1 "#judgments = %d\n" (number_of_judgments tj);
  print_judgments tj;
  Log.println 3 "";
  print_nttc nttc;
  Log.println 3 ""

let print_stmap stmap =
  Log.println 3 "stmap:";
  Map.iter
    (fun f states ->
       Set.iter
         (fun st -> Log.printf 3 "%s : %s\n" f st)
         states
    )
    stmap

let rev_flow flow =
  Map.foldi
    (fun x terms mp ->
       Set.fold
         (fun mp (Flow.Flow (g,n)) ->
            Map.modifyDefault
              Set.empty
              g
              (Set.add (x,n))
              mp
         )
         mp
         terms
    )
    flow
    Map.empty

let check hes lts ast=
  try
    Log.println 1 (Hes.to_string hes);
    Log.println 1 (Lts.to_string lts);
    Log.println 1 "Computing flow";
    let flow  = Flow.flow hes in
    let flow2 = rev_flow flow in
    let stmap = Saturation.stmap hes lts in 
    let tj    = Saturation.init hes lts ast in
    let toprop= Set.elements (Set.remove "$FF" (Hes.keys hes)) in
    let nttc  = judgs_to_map tj in
    let vartc = create_vartc tj flow2 in
    let vartc = update_vartc flow2 "$FF" (Set.singleton (Atype.Q "FFF")) vartc in
    let f_dep = forward_dependency hes flow in
    Profile.start "mainloop";
    let result = 
      for_
        (List.of_enum (1--10000)) 
        (tj, nttc, vartc, toprop)
        begin
          fun k (tj, nttc, vartc, toprops) ->
            match toprops with
            | [] -> 
              let res =
                let () = 
                  Log.println 1 "Solving parity game..." 
                in
                match ParityGame.create 
                        hes lts 
                        (ParityGame.priorMap ast)
                        (convert_judgments tj) with
                | None       -> false
                | Some graph -> 
                  ParityGame.solve "pgsolver" graph
              in
              Log.printf 0 
                "#type = %d\n" 
                (Set.cardinal (judgs_to_bindings tj));
              Log.printf 0 "#loop = %d\n" k;
              print_stats tj nttc;
              Break res
            | toprop :: toprops -> 
              begin
                (*
                Log.printf 1 "\nloop %d:\n" k;

                List.iter 
                  (fun f -> Log.printf 3 "%s, " f) 
                  (toprop :: toprops);

                Log.println 3 "";

                prerr_endline "-- nttc --";
                print_nttc  nttc;
                prerr_endline "";

                prerr_endline "-- vartc --";
                print_vartc vartc;
                prerr_endline "";
                *)

                let new_tj = 
                  (Saturation.propagate 
                     hes lts stmap nttc vartc toprop)
                in

                let newtypeset = 
                  Set.diff 
                    (Set.map fst new_tj) 
                    (Map.findDefault Set.empty toprop nttc)
                in

                let ntj = add_judgments tj toprop new_tj in

                (*
                prerr_endline "-- ntj --";
                print_judgments ntj;
                prerr_endline "---------";

                prerr_endline "-- mem --";
                Hashtbl.iter (fun (a,b,c) d -> prerr_endline (Atype.to_string a);prerr_endline (Term.to_string b);prerr_endline c;prerr_endline "") Saturation.memo;
                prerr_endline "---------";

                prerr_endline "-- newtypeset --";
                prerr_endline (string_of_int (Set.cardinal newtypeset));
                prerr_endline "---------";
                *)

                Profile.start "update_vartc";
                let vartc' = 
                  update_vartc flow2 toprop newtypeset vartc
                in
                Profile.finish "update_vartc";

                let nttc' = 
                  Map.modifyDefault 
                    Set.empty toprop (Set.union newtypeset) nttc
                in

                let ntoprop =
                  if Set.is_empty newtypeset 
                  then toprops
                  else
                    merge_toprop 
                      toprops 
                      (Set.elements (Map.find toprop f_dep))
                in

                Continue (ntj, nttc', vartc', ntoprop)
              end
        end
    in
    begin
      Profile.finish "mainloop";
      match result with
      | Some true  -> Safe
      | Some false -> Unsafe
      | None       -> Timeout
    end
  with e ->
    begin
      Log.println 0 (Printexc.to_string e);
      Log.println 0 (Printexc.get_backtrace ());
      Error
    end

