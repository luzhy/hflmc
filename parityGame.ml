open Batteries

open X

open Base
open Syntax
open Hes
open Lts

type vertex =
  | Exists of symbol * Atype.t * int
  | Forall of TypeEnv.t
  | ExistsDeadEnd
  | ForallDeadEnd

type edge = vertex * vertex

type t = vertex Set.t * edge Set.t * vertex * (vertex, int) Map.t

type int_game = int Set.t * (int * int) Set.t * int * (int, int) Map.t

let rec unions_unions = function
  | [] -> (Set.empty, Set.empty)
  | (x, y)::zs ->
    let x', y' = unions_unions zs in
    (Set.union x x', Set.union y y')

let rec divide_list = function
  | [] -> (Set.empty, Set.empty)
  | (x, y)::zs ->
    let x', y' = divide_list zs in
    (Set.add x x', Set.add y y')

let priorMap (rewriting_rule, _) =
  let rec loop tup  =
    match tup with
    | []                          -> (0, Map.empty)
    | (ntSymbol, ntType, _) :: tl -> 
      let lastTup = loop tl in
      let lastVal = fst lastTup in
      let lastMap = snd lastTup in
      match ntType with
      | Syntax.LFP ->
        if   lastVal mod 2 == 0 
        then (lastVal+1, Map.add ntSymbol (lastVal+1) lastMap)
        else (lastVal  , Map.add ntSymbol lastVal     lastMap)
      | Syntax.GFP ->
        if   lastVal mod 2 == 0 
        then (lastVal  , Map.add ntSymbol lastVal     lastMap)
        else (lastVal+1, Map.add ntSymbol (lastVal+1) lastMap)
  in
  snd (loop rewriting_rule)

let create 
    (Hes (rules, _, start_symbol, init_state))
    (Lts (delta, states, _)) 
    omegaMap
    type_judgments =

  Profile.start "graphconstruction";
  let e12, e21 =
    unions_unions
      (List.map
         (fun ((f, typ), env) ->
            let ee12 = 
              let m12 = Map.findDefault 0 f omegaMap in
              Set.singleton (Exists (f, typ, m12), Forall env)
            in
            let ee21 =
              Set.concat_map 
                (fun (f, ity) -> 
                   let m21 = Map.findDefault 0 f omegaMap in
                   Set.map 
                     (fun aty -> 
                        Forall env, Exists (f, aty, m21)) 
                     (Set.of_list (SSet.to_list ity)))
                env
            in
            (ee12, ee21))
         (Set.elements type_judgments))
  in
  let d1,  d2  = divide_list (Set.elements e12) in
  let d2', d1' = divide_list (Set.elements e21) in
  let v1 = Set.union d1 d1' in
  let v2 = Set.union d2 d2' in
  let init_node =
    Exists (start_symbol, 
            Atype.Q init_state, 
            Map.find start_symbol omegaMap)
  in
  let pf =
    Map.of_enum @@
    List.enum   @@
    List.concat [

      List.map
        (function Exists (_, _, m) as v -> (v, m) | 
           _ -> assert false)
        (Set.to_list v1);

      List.map
        (function Forall _ as v -> (v, 0) | 
           _ -> assert false)
        (Set.to_list v2);

      [(ExistsDeadEnd, 1); (ForallDeadEnd, 0)]]

  in
  let ans =
    if Set.mem init_node v1 then 
      (Some (Set.union v1 v2, Set.union e12 e21, init_node, pf))
    else 
      (None)
  in
  Profile.finish "graphconstruction";
  ans

let complete (vs, es, init_node, pf) =
  let add_es =
    Set.union
      (Set.map
         (function
           | Exists (_, _, _) as v ->
             (v, ExistsDeadEnd)
           | Forall _ as v ->
             (v, ForallDeadEnd)
           | _ ->
             invalid_arg "Dead ends must not be contained.")
         vs)
      (Set.of_list 
         [(ExistsDeadEnd, ExistsDeadEnd);
          (ForallDeadEnd, ForallDeadEnd)])
  in
  (List.fold_left 
     (flip Set.add) vs
     [ExistsDeadEnd; ForallDeadEnd], 
   Set.union es add_es, init_node, pf)

let to_int_game (vs, es, init_node, pf) =
  let mp = 
    Map.of_enum @@ 
    Enum.combine (Set.enum vs, 0 --^ Set.cardinal vs)
  in
  let find =
    flip Map.find mp
  in
  let v_to_who =
    Map.of_enum @@
    map
      (function
        | (Exists (_, _, _) | ExistsDeadEnd) as v -> (find v, 0)
        | (Forall _ | ForallDeadEnd) as v -> (find v, 1))
      (Set.enum vs)
  in
  let vs = Set.map find vs in
  let es = Set.map (Tuple2.map find find) es in
  let init_node = find init_node in
  let pf = Map.of_enum (map (first find) (Map.enum pf)) in
  (vs, v_to_who, es, init_node, pf)

let string_of_int_game (vs, v_to_who, es, init_node, pf) =
  String.join "\n" @@
  List.map (
    fun v ->
      Printf.sprintf
        "%d %d %d %s;"
        v
        (Map.find v pf)
        (Map.find v v_to_who)
        (String.join "," @@
         List.filter_map
           (fun (x, y) -> if x = v then Some (string_of_int y) else None)
           (Set.to_list es))
  )
    (Set.to_list vs)

let input_all_lines in_channel =
  let buf = DynArray.create () in (
    ignore_exceptions (fun () ->
        while true do
          DynArray.add buf (input_line in_channel)
        done) ();
    DynArray.to_list buf
  )

let solve cmd graph =
  Profile.start "paritygame";
  let (vs, v_to_who, es, init_node, pf) = to_int_game (complete graph) in
  let graph_str = string_of_int_game (vs, v_to_who, es, init_node, pf) in
  let (stdout, stdin) = Unix.open_process (String.join " " [cmd; "-global"; "bigstep"; "--printsolonly"])
  in
  output_string stdin graph_str;
  close_out stdin;
  let output = input_all_lines stdout
  in
  List.iter (fun line -> 
      Log.printf 4 "%s\n" line) output;
  let ans = 
    List.exists (fun line ->
        match String.nsplit (String.left line (String.length line - 1)) " " with
        | (v :: wl :: _) -> int_of_string v = init_node && int_of_string wl = 0
        | _ -> assert false
      )
      (List.drop 1 output)
  in
  Profile.finish "paritygame";
  ans
