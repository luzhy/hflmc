open Batteries

let first  f (x, y) = (f x,   y)
let second f (x, y) = (  x, f y)

let fst3 (x, _, _) = x
let snd3 (_, y, _) = y
let thd3 (_, _, z) = z

let first3  f (x, y, z) = (f x,   y,   z)
let second3 f (x, y, z) = (  x, f y,   z)
let third3  f (x, y, z) = (  x,   y, f z)

let ( ~~ ) x = flip x

let rec power = function
  | [] -> [[]]
  | x :: xs ->
      let yss = power xs in
      yss @ List.map (fun ys -> x :: ys) yss

(*$T power
  power [] = [[]]
  power [1;2] = [[]; [2]; [1]; [1;2]]
*)

let rec n_cartesian_product = function
  | [] -> [[]]
  | h :: t ->
      let rest = n_cartesian_product t in
      List.concat (List.map (fun i -> List.map (fun r -> i :: r) rest) h)

(*$T n_cartesian_product as ncp
  ncp []               = [[]]
  ncp [[]]             = []
  ncp [[1]; [2]; [3]]  = [[1;2;3]]
  ncp [[1;2;3]]        = [[1]; [2]; [3]]
  ncp [[1;2;3]; []]    = []
  ncp [[1;2;3]; [4;5]] = [[1;4]; [1;5]; [2;4]; [2;5]; [3;4]; [3;5]]
*)

let concat_map f lst =
  List.concat (List.map f lst)

let invalid_argf fmt =
  Printf.ksprintf invalid_arg fmt


module Set = struct
  include Set

  let map f set =
    of_enum (Enum.map f (enum set))

  let map_to f set =
    Map.of_enum (Enum.map (fun k -> (k, f k)) (enum set))

  let flatten sets =
    fold union sets empty

  let to_list set = elements set

  let fold f init set =
    fold (flip f) set init

  let concat_map f sets =
    flatten (Set.map f sets)
end


module Map = struct
  include Map

  let findDefault def key mp =
    try find key mp with Not_found -> def

  let modifyDefault def key f mp =
    modify key f (if mem key mp then mp else Map.add key def mp)

end

module List = struct
  include List

  let zip_with_index lst =
    let rec f i = function
      | [] -> []
      | x :: xs -> (i, x) :: f (i+1) xs
    in
    f 0 lst

  let iota n =
    List.init n identity

end


module SSet : sig
  type 'a t
  val empty : 'a t
  val singleton : 'a -> 'a t
  val add : 'a -> 'a t -> 'a t
  val remove : 'a -> 'a t -> 'a t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val map_monotonic : ('a -> 'b) -> 'a t -> 'b t
  val filter : ('a -> bool) -> 'a t -> 'a t
  val to_list : 'a t -> 'a list
  val of_list : 'a list -> 'a t
  val iter : ('a -> unit) -> 'a t -> unit
  val fold : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a
  val exists : ('a -> bool) -> 'a t -> bool
  val for_all : ('a -> bool) -> 'a t -> bool
  val is_empty : 'a t -> bool
  val choose : 'a t -> 'a
  val elements : 'a t -> 'a list
  val cardinal : 'a t -> int
  val union : 'a t -> 'a t -> 'a t
  val intersect : 'a t -> 'a t -> 'a t
  val of_enum : 'a Enum.t -> 'a t
  val filter_map : ('a -> 'b option) -> 'a t -> 'b t
  val unions : ('a t) list -> 'a t
  val mem : 'a -> 'a t -> bool
  val enum : 'a t -> 'a Enum.t
  val iter : ('a -> unit) -> 'a t -> unit
  val map_to : ('a -> 'b) -> 'a t -> ('a, 'b) Map.t
  val diff : 'a t -> 'a t -> 'a t
  val subset : 'a t -> 'a t -> bool
  val concats : 'a t t -> 'a t
  val concat_map : ('a -> 'b t) -> 'a t -> 'b t
end = struct

  type 'a t = 'a list

  let rec uniq = function
    | [] -> []
    | [x] -> [x]
    | x::y::zs -> 
      if x = y then
	uniq (x::zs)
      else
	x::uniq (y::zs)

  let us lst = uniq (List.sort compare lst)

  let empty = []
  let singleton x = [x]
  let rec add x = function
    | [] -> [x]
    | y :: ys -> 
      if x = y then
	y :: ys
      else if x < y then
	x :: y :: ys
      else
	y :: add x ys
  let rec remove x = function
    | [] -> invalid_arg "element not in the set."
    | y :: ys ->
      if x = y then
	ys
      else
	y :: remove x ys
  let map f set = us (List.map f set)
  let map_monotonic f set = List.map f set
  let filter f set = List.filter f set
  let to_list set = set
  let of_list list = us list
  let iter f set = List.iter f set
  let fold f init set = List.fold_left f init set
  let rec exists f = function
    | [] -> false
    | x :: xs -> if f x then true else exists f xs
  let rec for_all f = function
    | [] -> true
    | x :: xs -> if not (f x) then false else for_all f xs
  let is_empty = function
    | [] -> true
    | _ :: _ -> false
  let choose = function
    | [] -> invalid_arg "The set is empty. Cannot choose."
    | x :: _ -> x
  let elements set = to_list set
  let cardinal set = List.length set
  let rec union set1 set2 =
    match set1, set2 with
    | [], [] -> []
    | set1, [] -> set1
    | [], set2 -> set2
    | x :: xs, y :: ys -> 
      if x = y then
	x :: union xs ys
      else if x < y then
	x :: union xs set2
      else
	y :: union set1 ys
  let rec intersect set1 set2 =
    match set1, set2 with
    | [], [] -> []
    | [], _ -> []
    | _, [] -> []
    | x :: xs, y :: ys ->
      if x = y then
	x :: intersect xs ys
      else if x < y then
	intersect xs set2
      else
	intersect set1 ys
  let of_enum enum =
    of_list (List.of_enum enum)
  let rec filter_map f = function
    | [] -> []
    | x :: xs ->
      match f x with
      | None -> filter_map f xs
      | Some x -> x :: filter_map f xs

  let rec unions = function
    | [] -> empty
    | x :: xs -> union x (unions xs)

  let rec mem a = function
    | [] -> false
    | x::xs ->
      if x < a then
	mem a xs
      else if x > a then
	false
      else
	true

  let enum t =
    List.enum (to_list t)

  let rec iter f = function
    | [] -> ()
    | x::xs -> f x; iter f xs

  let map_to f set =
    Map.of_enum (Enum.map (fun k -> (k, f k)) (enum set))

  let rec diff set1 set2 =
    match set1 with
    | [] -> []
    | x::xs -> begin
      match set2 with
      | [] -> x::xs
      | y::ys -> 
	if x = y then
	  diff xs ys
	else if x < y then
	  x :: diff xs ys
	else
	  diff (x::xs) ys
    end

  let subset set1 set2 =
    is_empty (diff set1 set2)

  let concats sets =
    fold union empty sets

  let concat_map f sets =
    concats (map f sets)

end

let unions lst =
  List.fold_left Set.union Set.empty lst

let unions2 set_of_sets =
  Set.fold Set.union Set.empty set_of_sets
  
