open Base
open Batteries

open X

open Term

let genMasks nonTerminals removed =
  Set.of_list (List.map (Set.union removed % Set.of_list) (power (Set.elements (Set.diff nonTerminals removed))))

let rec willAllDie graph nonTerminals removed =
  if Set.cardinal removed = Set.cardinal nonTerminals then
    true
  else
    let nextRemoved =
      Set.union removed (Set.filter (fun f -> Set.subset (Map.find f graph) removed) nonTerminals)
    in
    if Set.cardinal nextRemoved < Set.cardinal removed then
      (Log.println 4 "nonTerminals:";
       Set.iter (Log.println 4) nonTerminals;
       Log.println 4 "removed:";
       Set.iter (Log.println 4) removed;
       Log.println 4 "nextRemoved:";
       Set.iter (Log.println 4) nextRemoved;
       assert false)
    else if Set.cardinal nextRemoved = Set.cardinal removed then
      false
    else
      willAllDie graph nonTerminals nextRemoved

let rec greedy rules graph removed killed =
  let nonTerminals = Set.of_enum (Map.keys rules)
  in
  let removed' = Set.union removed (Set.filter (fun f -> Set.subset (Map.find f graph) removed) nonTerminals)
  in
  if Set.cardinal removed' = Set.cardinal removed then
    let rest = Set.diff nonTerminals removed'
    in
    if Set.is_empty rest then
      killed
    else
      let kill = Set.choose rest
      in
      greedy rules graph (Set.add kill removed) (Set.add kill killed)
  else
    greedy rules graph removed' killed

let coreRecFun rules removed =
  let graph = dependencyGraph rules
  in
  greedy rules graph removed Set.empty

let rec replaceNT rpl (Term (termType, symbol, children)) =
  let children' = List.map (replaceNT rpl) children
  in
  match termType with
  | NonTerm when Map.mem symbol rpl ->
    Term (NonTerm, Map.find symbol rpl, children')
  | _ ->
    Term (termType, symbol, children')

let rec dfs graph cur goal visited =
  if cur = goal then
    true
  else if Set.mem cur visited then
    false
  else
    let visited' = Set.add cur visited
    in
    Set.exists
      (fun f -> dfs graph f goal visited')
      (Map.find cur graph)

let recFun rules =
  let graph = dependencyGraph rules in
  Set.of_enum 
    (filter
       (fun f -> Set.exists (fun g -> dfs graph g f Set.empty) (Map.find f graph))
       (Map.keys rules))

let unfold rules =
  Log.println 1 "unfold";
  let coreRecFuns = coreRecFun rules Set.empty
  in
  Set.iter (fun f -> Log.printf 1 "%s," f) coreRecFuns;
  Log.println 1 "";
  Log.println 1 "coreRecFun OK";
  let recFuns = recFun rules
  in
  Set.iter (fun f -> Log.printf 1 "%s," f) recFuns;
  Log.println 1 "";
  Log.println 1 "recFun OK";
  [? Map : (f', (args', body')) | 
   (f, (args, body)) <- Map.backwards rules;
   (f', (args', body')) <-
     let rpl1 = Set.map_to (fun f -> "#"^f) recFuns in
     let rpl2 = Set.map_to (fun f -> "##"^f) recFuns in
     let body1 = replaceNT rpl1 body in
     let body2 = replaceNT rpl2 body in
     if Set.mem f coreRecFuns then
       let body0 = (Term (TermOrr, "#or", [body; body1])) in
       let body3 = (Term (TermFFF, "#stop", [])) in
       List.enum [
         (f , (args, body0));
         ("#"^f, (args, body2));
         ("##"^f, (args, body3))
       ]
     else if Set.mem f recFuns then
       List.enum [(f, (args, body)); ("#"^f, (args, body1)); ("##"^f, (args, body2))]
     else
       Enum.singleton (f, (args, body))
         ?]

let modifyAPT (Lts.lts (delta, states, omega)) =
  let delta' = 
    Set.fold
      (fun delta' q -> Map.add (q, "#stop") (Formula.Ff) (Map.add (q, "#or") (Formula.Or ((Formula.P (1, q)), (Formula.P (2, q)))) delta'))
      delta
      states
  in
  Apt.Apt (delta', states, omega)

let modifyAPT2 (Apt.Apt (delta, states, omega)) =
  let delta' = Set.fold (fun delta' q -> (Map.add (q, "#or") (Formula.P (1, q)) delta')) delta states
  in
  Apt.Apt (delta', states, omega)
