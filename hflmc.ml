open Batteries

open Base
open Hflconfig

open Cmdliner

let main nocheck_safe nocheck_unsafe noopt_erase noopt_weakproof stats verbose pgsolver iter randomize step flow_precision conjunction =
  Profile.start "elapsed";
  Random.self_init ();
  Log.set_level verbose;
  let ast =
    try
      Ok (Hflparser.top Hfllexer.token (Lexing.from_channel stdin))
    with e ->
      Bad (!Hflparser.line)
  in
  match ast with
  | Bad line ->
    begin
      Log.printf 0 "Parse error at line %d.\n" line;
      print_endline "error"
    end
  | Ok ast ->
    try
      begin
        let () = Log.println 1 "Parsing input..." in
        let hes, lts = to_Hes_and_Lts ast in
        let result = ModelCheck.check hes lts ast in
        Profile.finish "elapsed";
        Profile.print_all ();
        print_endline (ModelCheck.Result.to_string result)
      end
    with 
      SortCheck.UnificationError msg -> 
      begin
        Log.println 0 "Sort checking failed.";
        Log.println 0 msg;
        print_endline "error"
      end

let nocheck_safe =
  let doc = "Not check for original APT." in
  Arg.(value & flag & info ["nocheck-safe"] ~doc)

let nocheck_unsafe =
  let doc = "Not check for complement APT." in
  Arg.(value & flag & info ["nocheck-unsafe"] ~doc)

let noopt_erase =
  let doc = "Not use optimized erase function." in
  Arg.(value & flag & info ["noopt-erase"] ~doc)

let noopt_weakproof =
  let doc = "Not use optimized parity game." in
  Arg.(value & flag & info ["noopt-weakproof"] ~doc)

let stats =
  let doc = "Show detailed stats." in
  Arg.(value & flag & info ["stats"] ~doc)

let verbose =
  let doc = "Show debug information." in
  Arg.(value & opt int 0 & info ["v"; "verbose"] ~doc)

let pgsolver =
  let doc = "Use the specified pgsolver executable." in
  Arg.(value & opt string "pgsolver" & info ["pgsolver"] ~doc)

let iter =
  let doc = "Iterates VAL times." in
  Arg.(value & opt int 1000 & info ["iter"] ~doc)

let randomize =
  let doc = "Use randomized algorithm to extract type candidates" in
  Arg.(value & flag & info ["randomize"] ~doc)

let step =
  let doc = "Change the number of expansion in an iteration" in
  Arg.(value & opt int 1 & info ["step"] ~doc)

let flow_precision =
  let doc = "Change the presicion of control flow analysis." in
  Arg.(value & opt int 1 & info ["flow-precision"] ~doc)

let conjunction =
  let doc = "Throw types of order less than VAL away" in
  Arg.(value & opt int 100 & info ["conjunction"] ~doc)

let cmd =
  let doc = "full LTS model checker for HES." in
  let man = [
    `S "DESCRIPTION";
    `P "$(tname) is a full LTS model checker for HES.";
    `S "BUGS";
    `P ""
  ]
  in
  Term.(pure main $ nocheck_safe $ 
        nocheck_unsafe $ noopt_erase $ 
        noopt_weakproof $ stats $ verbose $ 
        pgsolver $ iter $ randomize $ step $ 
        flow_precision $ conjunction),
  Term.info "hflmc" ~version:"0.1.0" ~doc ~man

let () = match Term.eval cmd with `Error _ -> exit 1 | _ -> exit 0
