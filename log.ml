let __level = ref 4

let set_level level =
  __level := level

let println level str =
  if level <= !__level then
    prerr_endline str

let newline level () =
  if level <= !__level then
    prerr_newline ()

let printf level fmt =
  Printf.ksprintf (if level <= !__level then (fun s -> prerr_string s; flush stderr) else ignore) fmt


