open Batteries

open X

open Base
open Term
open Hors
open Apt
open Formula

type t = TypeBinding.t * TypeEnv.t

let to_string (binding, env) =
  TypeEnv.to_string env ^ " |- " ^ TypeBinding.to_string binding

let memo = Hashtbl.create 0

(* term が atype を持つかどうか判定するだけ。高速化のために使う。 *)
let rec check (Apt (delta, _, _) as apt) typeCandidates varTypes atype (Term (termType, symbol, children) as term) =
  if Hashtbl.mem memo (atype, term) then
    Hashtbl.find memo (atype, term)
  else
    let ans = 
      match termType with
      | Terminal ->
	begin match atype with
	| Atype.A (_, _) -> (prerr_endline (Term.to_string term); assert false)
	| Atype.Q q ->
	  let rec f = function
            | Tt -> true
            | Ff -> false
            | P (i, q') -> check apt typeCandidates varTypes (Atype.Q q') (List.nth children (i-1))
            | And (f1, f2) -> f f1 && f f2
            | Or  (f1, f2) -> f f1 || f f2
	  in
	  f (Map.find (q, symbol) delta)
	end
      | NonTerminal ->
	let headTypeCandidates =
	  Set.map snd (Set.filter ((=) symbol % fst) typeCandidates)
	in
	check' apt typeCandidates varTypes atype symbol children headTypeCandidates
      | Variable ->
	let headTypeCandidates =
	  (Map.find symbol varTypes)
	in
	check' apt typeCandidates varTypes atype symbol children headTypeCandidates
    in
    (Hashtbl.add memo (atype, term) ans; ans)

and check' apt typeCandidates varTypes atype symbol children headTypeCandidates =
  let n = List.length children
  in
  Set.exists (fun headType ->
    let retType = Atype.drop n headType
    in
    let argTypes = Atype.take n headType
    in
    retType = atype && List.for_all (fun (child, itype) -> Set.for_all (fun (t, _) -> check apt typeCandidates varTypes t child) itype) (List.combine children argTypes)
  ) headTypeCandidates

let filterTypes' (Hors (rules, _, _, _)) apt typeCandidates =
  Set.filter
    (fun (symbol, atype) ->
      Log.printf 5 "symbol = %s, atype = %s" symbol (Atype.to_string atype);
      let argSymbols, term = Map.find symbol rules
      in
      let varTypes = 
	Map.of_enum 
	  (List.enum 
	     (List.combine 
		argSymbols 
		(List.map (Set.map fst) (Atype.init atype))))
      in
      Hashtbl.clear memo;
      prerr_string ".";
      flush stderr;
      check apt typeCandidates varTypes (Atype.Q (Atype.last atype)) term)
    typeCandidates

let rec filterTypes hors apt typeCandidates =
  let ans = filterTypes' hors apt typeCandidates in
  if Set.cardinal ans = Set.cardinal typeCandidates then
    ans
  else
    filterTypes hors apt ans

exception Empty

let memo2 = Hashtbl.create 0

(* term が atype を持つようなTypeEnvの集合を返す。 *)
let rec typeEnvs (Apt (delta, _, omega) as apt) typeCandidates varTypes nt atype (Term (termType, symbol, children) as term) =
  if Hashtbl.mem memo2 (nt, atype, term) then
    Hashtbl.find memo2 (nt, atype, term)
  else
    let ans = 
      match termType with
      | Terminal ->
	begin match atype with
	| Atype.A (_, _) -> (prerr_endline (Term.to_string term); prerr_endline (Atype.to_string atype); assert false)
	| Atype.Q q ->
	  let rec f = function
            | Tt -> Set.singleton Set.empty
            | Ff -> Set.empty
            | P (i, q) -> typeEnvs apt typeCandidates varTypes nt (Atype.Q q) (Log.printf 5 "%d\n" i; List.nth children (i-1))
            | And (f1, f2) -> [? Set : Set.union set1 set2 | set1 <- Set.enum (f f1); set2 <- Set.enum (f f2) ?]
            | Or (f1, f2) -> Set.union (f f1) (f f2)
	  in
	  Set.map (fun env -> TypeEnv.update (Map.find q omega) env) (f (Map.find (q, symbol) delta))
	end
      | NonTerminal ->
	let headTypeCandidates = (Set.map snd (Set.filter ((=) symbol % fst) typeCandidates))
	in
	typeEnvs' apt typeCandidates varTypes nt atype symbol children headTypeCandidates
      | Variable ->
	typeEnvs' apt typeCandidates varTypes nt atype symbol children (Map.find symbol varTypes)
    in
    (Hashtbl.add memo2 (nt, atype, term) ans; ans)

and typeEnvs' (Apt (_, _, omega) as apt) typeCandidates varTypes nt atype symbol children headTypeCandidates =
  let n = List.length children
  in
  [? Set : Set.add (symbol, (headType, (Map.find (Atype.last headType) omega))) env |
    headType <- Set.enum headTypeCandidates;
    Atype.drop n headType = atype;
    env <- 
      try
	List.enum
	  (List.map
             unions
             (n_cartesian_product
		(List.mapi
		   (fun i itype ->
                     List.map
                       unions
                       (n_cartesian_product
			  (List.map
                             (fun (t, m) ->
			       let env = (Set.elements (typeEnvs apt typeCandidates varTypes nt t (List.nth children i)))
			       in
			       if List.is_empty env then
				 raise Empty
			       else
				 List.map (TypeEnv.update m) env)
                             (Set.elements itype))))
		   (Atype.take n headType))))
      with Empty ->
	Enum.empty ()
  ?]

let tAbs argSymbols varTypes env =
  List.fold_right (fun symbol env -> Set.filter ((<>)symbol % fst) env) argSymbols env

let rec nonTerminals (Term.Term (termType, symbol, children)) =
  let rest = unions (List.map nonTerminals children)
  in
  match termType with
  | Term.NonTerminal -> Set.add symbol rest
  | _ -> rest

(* type judgmentを列挙する。本当は普通に全列挙すればよいが、引数の型を全探索していると(k-1)重指数で死ぬので、型候補ごとに、引数の型を固定して列挙している。 *)
let typeJudgments (Hors.Hors (rules, _, _, _)) apt tc toJudge =
  (concat_map
     (fun (symbol, atyp) ->
       let args, body = Map.find symbol rules
       in
       let varTypes =
         if List.length args <> List.length (Atype.init atyp) then
           (prerr_endline symbol; 
	    prerr_endline (Atype.to_string atyp); 
	    assert false)
         else
           Map.of_enum
             (List.enum
                (List.combine
                   args
                   (List.map
		      (Set.map fst)
			(Atype.init atyp))))
       in
       Hashtbl.clear memo2;
       Log.printf 1 "*";
       [? List : ((symbol, atyp), env) |
         env <-
           map
             (tAbs args varTypes)
             (Set.enum (typeEnvs apt (Set.union tc toJudge) varTypes symbol (Atype.Q (Atype.last atyp)) body))
       ?])
       (Set.elements toJudge))

