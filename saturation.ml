open Batteries
open X

open Base
open Hes
open Lts
open Term
open Sort

module TypeEnv = struct

  type t = (symbol * (Itype.t * Flow.t SSet.t)) list

  let empty =
    []

  let eraseFlowInfo env =
    List.map (second fst) env

  exception Conflict

  let rec add symbol aty flow = function
    | [] -> [(symbol, (Itype.singleton aty, flow))]
    | ((x, (ity, flow')) as h) :: renv ->
      if symbol = x then
        let flow'' = SSet.intersect flow flow'
        in
        if SSet.is_empty flow'' then
          raise Conflict
        else
          ((x, (Itype.add aty ity, flow'')) :: renv)
      else if symbol < x then
        (symbol, (Itype.singleton aty, flow)) :: h :: renv
      else
        h :: add symbol aty flow renv

  let rec union env1 env2 =
    match env1, env2 with
    | [], env2 -> env2
    | env1, [] -> env1
    | (x1, (ity1, flow1)) :: renv1, (x2, (ity2, flow2)) :: renv2 ->
      if x1 < x2 then
        (x1, (ity1, flow1)) :: union renv1 env2
      else if x2 < x1 then
        (x2, (ity2, flow2)) :: union env1 renv2
      else 
        let flow3 = SSet.intersect flow1 flow2
        in
        if SSet.is_empty flow3 then
          raise Conflict
        else
          (x1, (Itype.union ity1 ity2, flow3)) :: union renv1 renv2

  let unions envs =
    List.fold_left (fun env env2 -> union env env2) empty envs

  let print env =
    Log.printf 1 "{ ";
    List.iter 
      (fun (f, (ity, flow)) ->
         SSet.iter
           (fun aty ->
              Log.printf 1 "%s : %s, " f (Atype.to_string aty))
           ity)
      env;
    Log.printf 1 " }"

end

let stmap (Hes (rules, sorts, ss, q0)) (Lts.Lts (delta, states, _)) =
  let ntList = List.of_enum (Map.keys rules) in
  List.fold_right
    (fun a b ->
       if a <> ss then
         Map.add a states b
       else
         b)
    ntList
    (Map.singleton ss (Set.singleton q0))

let init 
    (Hes.Hes(rules,_, _, _)) 
    (Lts.Lts(_, states, _))
    (rewriting_rules, _) = 
  let sortMap   = fst (SortCheck.sortCheck rules) in
  let rec createAtype sort q =
    match sort with
    | Sort.A _     -> assert(false)
    | Sort.B       -> Atype.Q q
    | Sort.F(a, b) -> Atype.A ((SSet.empty), createAtype b q)
  in
  let createAtMap nt sort stset =
    Map.singleton 
      nt
      (Set.fold
         (fun m q ->
            Map.add (createAtype sort q) Set.empty m)
         Map.empty
         stset)
  in
  List.fold_right
    (fun (a,b,_) c ->
       let asort = Map.find a sortMap in
       if   b = Syntax.GFP  (*&& a <> (fst3 (List.hd rewriting_rules))*)
       then (Map.union (createAtMap a asort states) c)
       else c)
    rewriting_rules 
    Map.empty


let rec tAbs args env typ =
  let rec recur = function
    | [] -> typ
    | x :: xs ->
      Atype.A
        ((try (List.assoc x env) with Not_found -> Itype.empty), 
         (recur xs))
  in 
  recur args

exception Empty

let rec map_unions_n_cartesian_product_opt = function
  | [] -> Set.singleton TypeEnv.empty
  | x :: xs ->
    let rest = map_unions_n_cartesian_product_opt xs
    in
    unions2
      (Set.map
         (fun env ->
            Set.filter_map
              (fun env' ->
                 try
                   Some (TypeEnv.union env env')
                 with TypeEnv.Conflict ->
                   None)
              rest)
         x)

let memo : ((Atype.t * Term.t * symbol), TypeEnv.t Set.t) Hashtbl.t = Hashtbl.create 0

let rec typeEnvs 
    (Lts (delta, _, _) as lts)
    typeCandidates vtss nt atype
    (Term (termType, symbol, children) as term) =

  if   Hashtbl.mem  memo (atype, term, nt) 
  then Hashtbl.find memo (atype, term, nt)
  else
    let ans = 
      match termType with
      | TermTTT ->
        begin 
          match atype with
          | Atype.A (_, _) -> 
            (prerr_endline (Term.to_string term);
             prerr_endline (Atype.to_string atype); 
             assert false)
          | Atype.Q q ->
            Set.singleton TypeEnv.empty
        end

      | TermFFF ->
        begin 
          (prerr_endline (Term.to_string term);
           prerr_endline (Atype.to_string atype); 
           assert false)
        end

      | TermBox ->
        begin 
          let act = snd (String.split symbol "_") in
          match atype with
          | Atype.A (_, _) -> 
            (prerr_endline (Term.to_string term);
             prerr_endline (Atype.to_string atype); 
             assert false)
          | Atype.Q q ->
            let qset = (Map.findDefault Set.empty (q, act) delta) in
            if Set.is_empty qset
            then
              let flowset =
                SSet.unions 
                  (SSet.to_list
                     (SSet.unions
                        (List.map 
                           (fun x -> SSet.of_enum (Map.values x))
                           (List.of_enum (Map.values vtss)))))
              in
              if   SSet.mem (Flow.Flow ("$FF", 0)) flowset
              then Set.singleton TypeEnv.empty
              else Set.empty
            else
              Set.fold
                (fun a q ->
                   let f =
                     typeEnvs 
                       lts typeCandidates vtss nt (Atype.Q q)
                       (List.hd children)
                   in
                   unions2
                     (Set.map
                        (fun env1 ->
                           Set.filter_map
                             (fun env2 ->
                                try Some (TypeEnv.union env1 env2)
                                with TypeEnv.Conflict -> None)
                             a)
                        f))
                (Set.singleton TypeEnv.empty)
                qset
        end

      | TermDmd ->
        begin 
          let act = snd (String.split symbol "_") in
          match atype with
          | Atype.A (_, _) -> 
            (prerr_endline (Term.to_string term);
             prerr_endline (Atype.to_string atype); 
             assert false)
          | Atype.Q q ->
            let qset = 
              (Map.findDefault Set.empty (q, act) delta) 
            in
            Set.fold
              (fun a q ->
                 let f =
                   typeEnvs 
                     lts typeCandidates vtss nt (Atype.Q q)
                     (List.hd children)
                 in
                 Set.union f a)
              Set.empty
              qset
        end

      | TermAnd ->
        begin 
          let f1 = 
            typeEnvs
              lts typeCandidates vtss nt atype
              (List.nth children 0)
          in
          let f2 =
            typeEnvs
              lts typeCandidates vtss nt atype
              (List.nth children 1)
          in
          if   Set.is_empty f1 || Set.is_empty f2
          then Set.empty
          else
            unions2
              (Set.map
                 (fun env1 ->
                    Set.filter_map
                      (fun env2 ->
                         try Some (TypeEnv.union env1 env2)
                         with TypeEnv.Conflict -> None)
                      f2)
                 f1)
        end

      | TermOrr ->
        begin 
          let f1 = 
            typeEnvs
              lts typeCandidates vtss nt atype
              (List.nth children 0)
          in
          let f2 =
            typeEnvs
              lts typeCandidates vtss nt atype
              (List.nth children 1)
          in
          Set.union f1 f2
        end

      | NonTerm ->
        typeEnvs1 
          lts typeCandidates vtss nt atype symbol children
          (Map.findDefault Set.empty symbol typeCandidates)

      | Variabl ->
        typeEnvs2 
          lts typeCandidates vtss nt atype symbol children 
          (Map.findDefault Map.empty symbol vtss)
    in
    (Hashtbl.add memo (atype, term, nt) ans; ans)

and typeEnvs1 
    (Lts (_, _, _) as lts) 
    typeCandidates vtss nt atype symbol children headTypeCandidates =
  let n = List.length children in
  unions2
    (Set.map
       (fun headType ->
          if Atype.drop n headType <> atype then
            Set.empty
          else
            Set.filter_map
              (fun env -> 
                 (try 
                    Some (TypeEnv.add symbol headType 
                            (SSet.singleton 
                               (Flow.Flow ("#dummy", -1))) env) 
                  with 
                    TypeEnv.Conflict -> None))
              (try
                 map_unions_n_cartesian_product_opt
                   (List.mapi 
                      (fun i itype ->
                         map_unions_n_cartesian_product_opt
                           (List.map
                              (fun t ->
                                 let envs = 
                                   (typeEnvs 
                                      lts typeCandidates vtss nt t 
                                      (List.nth children i))
                                 in
                                 if   Set.is_empty envs 
                                 then raise Empty
                                 else envs)
                              (SSet.elements itype)))
                      (Atype.take n headType))
               with 
                 Empty -> Set.empty))
       headTypeCandidates)

and typeEnvs2
    (Lts (_, _, _) as lts) 
    typeCandidates vtss nt atype symbol children headTypeCandidates =
  let n = List.length children in
  unions
    (List.map
       (fun (headType, inflow) ->
          if Atype.drop n headType <> atype then
            Set.empty
          else
            Set.filter_map
              (fun env -> 
                 (try 
                    Some (TypeEnv.add symbol headType inflow env)
                  with 
                    TypeEnv.Conflict -> None))
              (try
                 map_unions_n_cartesian_product_opt
                   (List.mapi 
                      (fun i itype ->
                         map_unions_n_cartesian_product_opt
                           (List.map
                              (fun t ->
                                 let envs = 
                                   (typeEnvs 
                                      lts typeCandidates vtss nt t 
                                      (List.nth children i))
                                 in
                                 if Set.is_empty envs
                                 then raise Empty
                                 else envs)
                              (SSet.elements itype)))
                      (Atype.take n headType))
               with 
                 Empty -> Set.empty))
       (Map.bindings headTypeCandidates))

let tAbsEnv args env =
  List.filter (fun (f, _) -> not (List.mem f args)) env

let propagate 
    (Hes (rules, _, _, _)) 
    (Lts (_, states, _) as lts) 
    stmap nttc vartc toPropagate =

  Hashtbl.clear memo; 
  Profile.start "bpropagate_all";
  let (args, body) = Map.find toPropagate rules in
  let vtss = (Map.findDefault Map.empty toPropagate vartc) in
  Log.printf 1 "Propagating %s\n" toPropagate;
  let ans = 
    unions2
      (Set.map 
         (fun q ->
            Log.printf 1 "Propagating with type %s\n" q;
            Profile.start "bpropagate";
            let envs = (typeEnvs lts nttc vtss toPropagate (Atype.Q q) body) in
            Profile.finish "bpropagate";
            Set.map
              (fun env ->
                 let env = TypeEnv.eraseFlowInfo env in
                 (tAbs args env (Atype.Q q), 
                  Set.of_list (tAbsEnv args env)))
              envs) 
         (Map.findDefault Set.empty toPropagate stmap))
  in
  Profile.finish "bpropagate_all";
  ans

