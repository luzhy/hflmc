open Unix

type t = (string, float) Hashtbl.t

let tbl_start : t = Hashtbl.create 0
let tbl : t = Hashtbl.create 0

let start key = Hashtbl.replace tbl_start key (Unix.gettimeofday ())

let finish key =
  let prev =
    if not (Hashtbl.mem tbl key) then 0.0 else Hashtbl.find tbl key
  in
  Hashtbl.replace tbl key (prev +. (Unix.gettimeofday () -. Hashtbl.find tbl_start key))

let print_all () =
  Hashtbl.iter
    (Printf.eprintf "%s: %.3f\n")
    tbl
