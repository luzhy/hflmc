
type thflV =
  | HflV of string
  | HflN of string

type tType =
  | TPr
  | TAp of tType * tType

type vType =
  | LFP
  | GFP

type hflFormula = 
  | TTT
  | FFF
  | Var of thflV
  | Orr of hflFormula * hflFormula
  | And of hflFormula * hflFormula
  | Box of string * hflFormula
  | Dmd of string * hflFormula
  | Lam of thflV * tType * hflFormula
  | App of hflFormula * hflFormula

type transition = string * string * string

type top = ((string * vType * hflFormula) list) * (transition list)

