%{
  open Syntax
  let line = ref 0
%}


%token <string> TUIdent
%token <string> TLIdent

%token TFalse 
%token TTrue  
%token TPrType 
%token TZero 
%token TPlus 
%token TMinu 
%token TAnd 
%token TOrr 
%token TLambda 
%token TArrow  
%token TSemiColon 
%token TDot 
%token TGfp 
%token TLfp 
%token TLeftABrack  
%token TRightABrack 
%token TLeftSBrack  
%token TRightSBrack 
%token TLeftParen  
%token TRightParen 
%token TComma 
%token TEq 
%token TGRAMMAR 
%token TTRANSITION 
%token TEOF 

%start top
%type <Syntax.top> top
%%

top:
  | TGRAMMAR hes TTRANSITION lts TEOF
    { ($2, $4) }

hes:
  | hesFormula TDot hes 
    { $1 :: $3 }
  | hesFormula TDot
    { $1 :: [] }

hesFormula:
  | TUIdent TEq TGfp hflFormula 
    { ($1, GFP, $4)      }
  | TUIdent TEq TLfp hflFormula 
    { ($1, LFP, $4)      }


hflFormula:
  | TLambda TLIdent TSemiColon tType TDot hflFormula 
    { Lam (HflV $2, $4, $6)     }
  | phflFormula 
    { $1                        }


phflFormula:
  | phflFormula pphflFormula
        { App ($1, $2)               }
  | pphflFormula 
        { $1                         }

pphflFormula:
  | pphflFormula TOrr ppphflFormula 
        { Orr($1, $3) }
  | pphflFormula TAnd ppphflFormula 
        { And($1, $3) }
  | ppphflFormula 
        { $1        }

ppphflFormula:
  | TLeftSBrack TLIdent TRightSBrack ppphflFormula { Box($2, $4) }
  | TLeftABrack TLIdent TRightABrack ppphflFormula { Dmd($2, $4) }
  | pppphflFormula { $1 }

pppphflFormula:
  | TUIdent { Var (HflN $1) }
  | TLIdent { Var (HflV $1) }
  | TTrue   { TTT             }
  | TFalse  { FFF             }
  | TLeftParen hflFormula TRightParen { $2 }

tType :
  | ptType 
        { $1           }
  | ptType TArrow tType 
        { TAp($1, $3) }

ptType :
  | TPrType          
    { TPr }
  | TLeftParen tType TRightParen 
    { $2  }

lts :
  | transTrip TDot lts { $1 :: $3 }
  | transTrip TDot     { [$1] }

transTrip :
  | TLeftParen TLIdent TComma TLIdent TComma TLIdent TRightParen
    { ($2, $4, $6) }
