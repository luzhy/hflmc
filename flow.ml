open Batteries
open X

open Base
open Hes
open Lts

open Term

type t = Flow of string * int

type cTH =
  | CF of symbol
  | CX of (symbol * symbol)

let cTHToString = function
  | CF symbol -> symbol
  | CX (f, x) -> f ^ "$" ^ x

type t' = cTH * int

type constr = Constr of cTH * int * t' (* nth child of `head` is `term`. *)

let var_to_string (f, x) = f ^ "." ^ x 

let to_string (head, n) =
  cTHToString head ^ " " ^ String.join " " (List.make n "*")

let constrToString (Constr (cTermHead, n, flow)) =
  cTHToString cTermHead ^ "@" ^ string_of_int n ^ " <- " ^ to_string flow

let print level flow =
  Map.iter
    (fun (f, x) set ->
       Set.iter (fun (Flow (g, n)) -> Log.printf level "%s.%s <- %s@%d\n" f x g n) set
    )
    flow

let convert curNT (Term (termType, symbol, children)) =
  let cth =
    match termType with
    | TermTTT
    | TermFFF
    | TermBox
    | TermDmd
    | TermAnd
    | TermOrr    -> invalid_arg "Cannot convert terminals."
    | NonTerm    -> CF symbol
    | Variabl    -> CX (curNT, symbol)
  in
  (cth, List.length children)

let rec genConstraintsTerm 
    (Hes (rewritingRules, _, _, _) as hesInst) 
    curNT 
    (Term (termType, symbol, children)) =
  let rest = unions (List.map (genConstraintsTerm hesInst curNT) children)
  in
  Set.union
    rest
    (match termType with
     | TermTTT
     | TermFFF
     | TermBox
     | TermDmd
     | TermAnd
     | TermOrr ->
       Set.empty
     | NonTerm ->
       Set.of_list
         (List.map
            (fun (i, child) -> Constr (CF symbol, i, convert curNT child))
            (List.zip_with_index children))
     | Variabl ->
       Set.of_list
         (List.map
            (fun (i, child) -> Constr (CX (curNT, symbol), i, convert curNT child))
            (List.zip_with_index children)))

let genConstraintsRule hesInst (symbol, (_, body)) =
  genConstraintsTerm hesInst symbol body

let genConstraints (Hes (rewritingRules, _, _, _) as hesInst) =
  unions (List.map (genConstraintsRule hesInst) (Map.bindings rewritingRules))

let initMap hesInst constraints =
  Set.fold 
    (fun mp (Constr (head, i, (head', n))) ->
       match head, head' with
       | CF f, CF g ->
         Map.modifyDefault
           Set.empty
           (Hes.getArg hesInst f i)
           (Set.add (Flow (g, n)))
           mp
       | _ -> mp)
    Map.empty
    constraints

let initTodo mp =
  Map.foldi
    (fun f set todo -> 
       (List.map (fun flow -> (f, flow)) (Set.to_list set))@todo)
    mp
    []

let add arg flow (mp, todo) =
  if Set.mem flow (Map.findDefault Set.empty arg mp) then
    (mp, todo)
  else
    let mp = Map.modifyDefault Set.empty arg (Set.add flow) mp in
    let todo = (arg, flow) :: todo in
    (mp, todo)

let p (f,x) h m =
  Log.printf 1 "=== DEBUG === %s@%s <- %s$%d\n" f x h m

let rec resolveConstraints' hesInst constraints = function
  | (mp, []) -> mp
  | (mp, (x, Flow (h, m))::todo) ->
    p x h m;
    let next =
      Set.fold
        (fun res (Constr (head, i, (head', n))) -> 
           match head, head' with
           | CF f, CF g -> res
           | CF f, CX z ->
             if z = x && Hes.getArg hesInst f i <> z then
               add (Hes.getArg hesInst f i) (Flow (h, m+n)) res
             else
               res
           | CX y, CF f ->
             if y = x then
               add (Hes.getArg hesInst h (m+i)) (Flow (f, n)) res
             else
               res
           | CX y, CX z ->
             let res' =
               if y = x then
                 Set.fold
                   (fun res (Flow (f, l)) -> 
                      (add (Hes.getArg hesInst h (m+i)) (Flow (f, l+n)) res))
                   res
                   (Map.findDefault Set.empty z mp)
               else
                 res
             in
             if z = x then
               Set.fold
                 (fun res' (Flow (f, l)) ->
                    let arg = Hes.getArg hesInst f (l+i) in
                    if arg <> z then
                      add arg (Flow (h, m+n)) res'
                    else
                      res'
                 )
                 res'
                 (Map.findDefault Set.empty y mp)
             else
               res'
        )
        (mp, todo)
        constraints
    in
    resolveConstraints' hesInst constraints next

(* resolveConstraints : constraints -> (var, (term_type * symbol * int) Set.t) Map.t *)
let resolveConstraints hesInst constraints =
  let mp = initMap hesInst constraints
  in
  resolveConstraints' hesInst constraints (mp, (initTodo mp))

let flow hesInst =
  let constraints = genConstraints hesInst
  in
  let () = Log.println 4 "Flow constraints enumerated." in
  let () = 
    Set.iter (fun cst -> Log.println 4 (constrToString cst)) constraints in
  resolveConstraints hesInst constraints

