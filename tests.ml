open Batteries
open Cmdliner
open Pa_comprehension

open X
open Log
open Profile
open Syntax
open Hflparser
open Hfllexer
open Base
open Flow
open Saturation


(*
let rev_flow flow =
  Map.foldi
    (fun x terms mp ->
       Set.fold
         (fun mp (Flow.Flow (g,n)) ->
            Map.modifyDefault
              Set.empty
              g
              (Set.add (x,n))
              mp
         )
         mp
         terms
    )
    flow
    Map.empty

let judgs_to_map tj =
  Map.map (fun ty_envs -> Set.of_enum (Map.keys ty_envs)) tj

let update_vartc hes flow2 g newtypeset vartc =
  Set.fold
    (fun vartc ((f,x), n) ->
       Map.modifyDefault
         Map.empty
         f
         (fun mp ->
            Set.fold
              (fun mp ty -> 
                 Map.modifyDefault
                   Map.empty
                   x
                   (Map.modifyDefault
                      SSet.empty
                      (Atype.drop n ty)
                      (SSet.add (Flow.Flow (g, n))))
                   mp)
              mp
              newtypeset
         ) 
         vartc
    )
    vartc
    (Map.findDefault Set.empty g flow2)

let exfile = open_in "exp2.hrs" ;;

let exast = Parser.top Lexer.token (Lexing.from_channel exfile) ;;

let exhes, exlts = to_Hes_and_Lts exast ;;

let exstmap = Saturation.stmap exhes exlts ;;

let exflow = Flow.flow exhes ;;

let exflow2 = rev_flow exflow ;;

let extj : (string, (Base.Atype.t, Base.TypeEnv.t X.Set.t) X.Map.t) X.Map.t = Saturation.init_s exhes exlts exast ;;

let extoprop  = Set.elements (Hes.keys exhes)

let exnttc = judgs_to_map extj

let exvartc = 
  Map.foldi 
    (fun f ty_envs -> 
       update_vartc exhes exflow2 f (Set.of_enum (Map.keys ty_envs)))
    extj 
    Map.empty
   *)



(*
let exconfig = { nocheck_safe = false; nocheck_unsafe = false; noopt_erase = false; noopt_weakproof = false; stats = false; verbose = 0; pgsolver = "pgsolver"; iter = 1000; randomize = false; step = 1; flow_precision = 1; conjunction = 100 }

let exfile = open_in "example2-1.hrs" ;;

let exast = Parser.top Lexer.token (Lexing.from_channel exfile) ;;

let _, exhors, exapt = hors_and_apt_of_syntax exast


let exhors = Hors.modifyRule (PreProcess.unfold) exhors

let exapt = PreProcess.modifyAPT exapt

let exflow  = Flow.flow exhors

let exflow2 = ModelCheck.rev_flow exflow

let exstmap = Saturation.stmap exhors exapt exflow

let extj : (string, (Base.Atype.t, Base.TypeEnv.t X.Set.t) X.Map.t) X.Map.t = Saturation.init exhors exapt exflow exstmap

let extoprop  = Set.elements (Hors.keys exhors)

let exnttc = ModelCheck.judgs_to_map extj

let exvartc = Map.foldi (fun f ty_envs -> ModelCheck.update_vartc exhors exflow2 f (Set.of_enum (Map.keys ty_envs))) extj Map.empty

*)

(*
open Term
open Sort
open Formula

let ta1 = [("S",
            ([],
             Term(NonTerminal, "X", [])));
           ("Y",
            (["z"], 
             Term(ExtOp, "a", 
                  [Term(AndOp, "/\\",
                        [Term(Variable, "z", []);
                         Term(NonTerminal, "X",[])])])));
           ("X",
            ([], 
             Term(ExtOp, "<a>", 
                  [Term(NonTerminal, "Y",
                        [Term(NonTerminal, "X", [])])])))]

let ta2 = [("S", Sort.B); ("Y", Sort.F(Sort.B, Sort.B)); ("X", Sort.B)]

let a1 = List.fold_right (fun (l, r) -> Map.add l r) ta1 Map.empty
let a2 = List.fold_right (fun (l, r) -> Map.add l r) ta2 Map.empty
let a3 = "S"
let a4 = "s0"
let exp1hors = Hors.Hors(a1, a2, a3, a4)

let tb1 = [(("s0", "a"), Formula.P(1, "s0"))]
let tb2 = ["s0"]
let tb3 = [("s0",0)]
let b1 = List.fold_right (fun (l, r) -> Map.add l r) tb1 Map.empty
let b2 = List.fold_right Set.add tb2 Set.empty
let b3 = List.fold_right (fun (l, r) -> Map.add l r) tb3 Map.empty
let exp1apt = Apt.Apt(b1, b2, b3)

let tc = [("S", "s0");
          ("Y", "s0");
          ("X", "s0")]
let exp1stmap = List.fold_right 
    (fun (l, r) -> Map.add l (Set.singleton r)) tc Map.empty

let exp1tj:(string,(Base.Atype.t,Base.TypeEnv.t X.Set.t) X.Map.t) X.Map.t 
  = Map.singleton "Y" (Map.singleton 
                         (Atype.A(SSet.empty, Atype.Q "s0")) 
                         Set.empty) 

let exp1nttc = ModelCheck.judgs_to_map exp1tj

let exp1vartc : (string, 
                 (string, 
                  (Atype.t, 
                   Flow.t SSet.t) Map.t) Map.t) Map.t
  = Map.singleton "Y"
    (Map.singleton "z"
       (Map.singleton (Atype.Q "s0")
          (SSet.singleton (Flow.Flow("S", 1)))))
;;

prerr_endline "";
prerr_endline "Test case 1";

prerr_endline "start hors";
prerr_endline (Hors.to_string exp1hors);
prerr_endline "end hors";
prerr_endline "";

prerr_endline "start stmap";
print_stmap exp1stmap;
prerr_endline "end stmap";
prerr_endline "";

prerr_endline "start vartc";
Saturation.print_vartc exp1vartc;
prerr_endline "end vartc";
prerr_endline "";

let rec forloop (tj, nttc, vartc, ntoprops) =
  match ntoprops with
  | [] -> print_stats tj nttc;
  | toprop :: toprops -> 
    begin
      List.iter (fun f -> Log.printf 3 "%s, " f) (toprop :: toprops);
      Log.println 3 "";

      let tj_proped = 
        (Saturation.propagate 
           exp1hors exp1apt exp1stmap nttc vartc toprop) in
      let newtypeset = 
        Set.diff
          (Set.map fst tj_proped) 
          (Map.findDefault Set.empty toprop nttc) in 
      let new_tj = add_judgments tj toprop tj_proped in
      let new_nttc = 
        Map.modifyDefault 
          Set.empty toprop (Set.union newtypeset) nttc in
      forloop (new_tj, new_nttc, vartc, toprops)
    end
in
forloop (exp1tj, exp1nttc, exp1vartc, ["X"; "Y"; "S"; "X"; "Y"; "S"; "X"; "Y"; "S"])
*)

(*
open Term

let taa1 = 
  [
    ("$<a>",
     (["x0"],
      Term(Terminal, "<a>", 
           [Term(Variable, "x0", [])])));

    ("$<b>",
     (["x0"],
      Term(Terminal, "<b>", 
           [Term(Variable, "x0", [])])));

    ("S",
     ([],
      Term(NonTerminal, "F", 
           [Term(NonTerminal, "B", [])])));

    ("F",
     (["x"], 
      Term(NonTerminal, "$<a>", 
           [Term(Variable, "x",
                 [Term(NonTerminal, "F",
                       [Term(NonTerminal, "G",
                             [Term(Variable, "x", [])])])])])));

    ("G",
     (["x";"y"], 
      Term(NonTerminal, "$<b>", 
           [Term(Variable, "x",
                  [Term(Variable, "y", [])])])));

    ("B",
     (["y"], 
      Term(NonTerminal, "$<b>", 
           [Term(Variable, "y",[])])))
  ]

let taa2 = [("S", Sort.B); 
            ("F", Sort.B);
            ("G", Sort.B); 
            ("B", Sort.B)]

let aa1 = List.fold_right (fun (l, r) -> Map.add l r) taa1 Map.empty
let aa2 = List.fold_right (fun (l, r) -> Map.add l r) taa2 Map.empty
let aa3 = "S"
let aa4 = "s0"
let exp2hors = Hors.Hors(aa1, aa2, aa3, aa4)


let tbb1 = [(("s0", "<a>"), Formula.P (1, "s1")); 
            (("s0", "<b>"), Formula.Tt);
            (("s1", "<a>"), Formula.Tt);
            (("s1", "<b>"), Formula.Or (Formula.P (1, "s0"),
                                        Formula.P (1, "s1")))]
let tbb2 = ["s0"; "s1"]
let tbb3 = [("s0", 0); ("s1", 0)]

let bb1 = List.fold_right (fun (l, r) -> Map.add l r) tbb1 Map.empty
let bb2 = List.fold_right Set.add tbb2 Set.empty
let bb3 = List.fold_right (fun (l, r) -> Map.add l r) tbb3 Map.empty
let exp2apt = Apt.Apt(bb1, bb2, bb3)
   *)

   (*
open Term
open Sort
open Formula

let ta1 = [("$<a>",
            (["x0"],
             Term(Terminal, "<a>", 
                  [Term (Variable, "x0", [])])));
           ("$and",
            (["x1";"x0"],
             Term(Terminal, "and", 
                  [Term (Variable, "x1", []);
                   Term (Variable, "x0", [])])));
           ("S",
            ([],
             Term(NonTerminal, "X", [])));
           ("Y",
            (["z"], 
             Term(NonTerminal, "$<a>", 
                  [Term(NonTerminal, "$and",
                        [Term(Variable,    "z", []);
                         Term(NonTerminal, "X", [])])])));
           ("X",
            ([], 
             Term(NonTerminal, "$<a>", 
                  [Term(NonTerminal, "Y",
                        [Term(NonTerminal, "X", [])])])))]

let ta2 = [("S", Sort.B); ("Y", Sort.F(Sort.B, Sort.B)); ("X", Sort.B)]

let a1 = List.fold_right (fun (l, r) -> Map.add l r) ta1 Map.empty
let a2 = List.fold_right (fun (l, r) -> Map.add l r) ta2 Map.empty
let a3 = "S"
let a4 = "s0"
let exp1hors = Hors.Hors(a1, a2, a3, a4)

let tb1 = [(("s0", "<a>"), Formula.P(1, "s0"));
           (("s0", "and"), Formula.And(Formula.P(1, "s0"),
                                       Formula.P(2, "s0")))]
let tb2 = ["s0"]
let tb3 = [("s0",0)]
let b1 = List.fold_right (fun (l, r) -> Map.add l r) tb1 Map.empty
let b2 = List.fold_right Set.add tb2 Set.empty
let b3 = List.fold_right (fun (l, r) -> Map.add l r) tb3 Map.empty
let exp1apt = Apt.Apt(b1, b2, b3)
   *)

   (*
let tc = [("S", "s0");
          ("Y", "s0");
          ("X", "s0")]
let exp1stmap = List.fold_right 
    (fun (l, r) -> Map.add l (Set.singleton r)) tc Map.empty

let exp1tj:(string,(Base.Atype.t,Base.TypeEnv.t X.Set.t) X.Map.t) X.Map.t 
  = Map.singleton "Y" (Map.singleton 
                         (Atype.A(SSet.empty, Atype.Q "s0")) 
                         Set.empty) 

let exp1nttc = ModelCheck.judgs_to_map exp1tj

let exp1vartc : (string, 
                 (string, 
                  (Atype.t, 
                   Flow.t SSet.t) Map.t) Map.t) Map.t
  = Map.singleton "Y"
    (Map.singleton "z"
       (Map.singleton (Atype.Q "s0")
          (SSet.singleton (Flow.Flow("S", 1)))))
;;

prerr_endline "";
prerr_endline "Test case 1";

prerr_endline "start hors";
prerr_endline (Hors.to_string exp1hors);
prerr_endline "end hors";
prerr_endline "";

prerr_endline "start stmap";
print_stmap exp1stmap;
prerr_endline "end stmap";
prerr_endline "";

prerr_endline "start vartc";
Saturation.print_vartc exp1vartc;
prerr_endline "end vartc";
prerr_endline "";

let rec forloop (tj, nttc, vartc, ntoprops) =
  match ntoprops with
  | [] -> print_stats tj nttc;
  | toprop :: toprops -> 
    begin
      List.iter (fun f -> Log.printf 3 "%s, " f) (toprop :: toprops);
      Log.println 3 "";

      let tj_proped = 
        (Saturation.propagate 
           exp1hors exp1apt exp1stmap nttc vartc toprop) in
      let newtypeset = 
        Set.diff
          (Set.map fst tj_proped) 
          (Map.findDefault Set.empty toprop nttc) in 
      let new_tj = add_judgments tj toprop tj_proped in
      let new_nttc = 
        Map.modifyDefault 
          Set.empty toprop (Set.union newtypeset) nttc in
      forloop (new_tj, new_nttc, vartc, toprops)
    end
in
forloop (exp1tj, exp1nttc, exp1vartc, ["X"; "Y"; "S"; "X"; "Y"; "S"; "X"; "Y"; "S"])
   *)

(*
let print_nttc nttc =
  Map.iter
    (fun f tys ->
       Set.iter 
         (fun ty -> Log.printf 3 "%s : %s\n" f (Atype.to_string ty))
         tys
    )
    nttc

let print_tj tj =
    Map.iter
      (fun f aty_envs -> 
         Map.iter
           (fun aty envs ->
              Set.iter
                (fun env ->
                   prerr_endline 
                     ((Base.TypeEnv.to_string env) ^ " |- " ^
                      f ^ " : " ^
                      (Atype.to_string aty)))
                envs)
           aty_envs)
      tj

let print_vartc v =
  Map.iter
    (fun a1 b1 ->
       prerr_endline a1;
       Map.iter 
         (fun a2 b2 ->
            prerr_endline ("- " ^ a2);
            Map.iter 
              (fun a3 b3 ->
                 prerr_endline ("-- " ^ (Atype.to_string a3));
                 SSet.iter
                   (fun (Flow.Flow (a4, b4)) ->
                      prerr_endline ("--- (" ^ a4 ^ ", " ^ (string_of_int b4) ^ " )"))
                   b3)
              b2)
         b1)
    v

let print_stmap stmap =
  Log.println 3 "stmap:";
  Map.iter
    (fun f states ->
       Set.iter
         (fun st -> Log.printf 3 "%s : %s\n" f st)
         states
    )
    stmap

let add_judgments tj nt tj_new =
  Set.fold 
    (fun tj' (ty, env) -> 
       Map.modify 
         nt
         (fun ty_envs ->
            Map.modify 
              ty (Set.add env) 
              (if   Map.mem ty ty_envs 
               then ty_envs
               else Map.add ty Set.empty ty_envs))
         (if   Map.mem nt tj' 
          then tj' 
          else Map.add nt Map.empty tj'))
    tj
    tj_new

let rec repeatlst i lst =
  if i == 0 then
    []
  else
    lst @ (repeatlst (i-1) lst)

let convert_judgments judgments =
  unions
    (List.map
       (fun (f, ty_envs) ->
          unions
            (List.map
               (fun (ty, envs) -> 
                  Set.map
                    (fun env -> ((f, ty), env))
                    envs)
               (Map.bindings ty_envs)))
       (Map.bindings judgments))

let final (tj, nttc, vartc, ntoprops) = 
  let rec forloop (tj, nttc, vartc, ntoprops) =
    match ntoprops with
    | [] -> tj
    | toprop :: toprops -> 
      begin
        prerr_endline toprop;

        print_endline "tj------------";
        print_tj tj;
        print_endline "nttc------------";
        print_nttc nttc;
        let tj_proped = 
          (Saturation.propagate 
             exhes exlts exstmap nttc vartc toprop) in

        let newtypeset = 
          Set.diff
            (Set.map fst tj_proped) 
            (Map.findDefault Set.empty toprop nttc) in 

        let new_tj = 
          add_judgments tj toprop tj_proped in

        print_endline "new_tj------------";
        print_tj new_tj;

        let new_vartc = 
          update_vartc exhes exflow2 toprop newtypeset vartc in

        let new_nttc = 
          Map.modifyDefault 
            Set.empty toprop (Set.union newtypeset) nttc in

        forloop (new_tj, new_nttc, new_vartc, toprops)
      end
  in
  forloop (extj, exnttc, exvartc,
           repeatlst 5 extoprop)

   *)

let tesetcheck hes lts ast=
  Log.println 1 (Hes.to_string hes);
  Log.println 1 (Lts.to_string lts);
  Log.println 1 "Computing flow";
  let flow  = Flow.flow hes in
  let flow2 = ModelCheck.rev_flow flow in
  let stmap = Saturation.stmap hes lts in 
  let tj    = Saturation.init hes lts ast in
  let toprop= Set.elements (Set.remove "$FF" (Hes.keys hes)) in
  let nttc  = ModelCheck.judgs_to_map tj in
  let vartc = ModelCheck.create_vartc tj flow2 in
  let vartc = ModelCheck.update_vartc flow2 "$FF" (Set.singleton (Atype.Q "FFF")) vartc in
  let f_dep = ModelCheck.forward_dependency hes flow in
  Profile.start "mainloop";
  let rec forloop k (tj, nttc, vartc, toprop) =
    match toprop with
    | [] -> (tj, nttc, vartc)
    | toprop :: toprops -> begin
        Log.printf 1 "\nloop %d:\n" k;

        List.iter 
          (fun f -> Log.printf 3 "%s, " f) 
          (toprop :: toprops);

        Log.println 3 "";

        prerr_endline "-- nttc --";
        ModelCheck.print_nttc  nttc;
        prerr_endline "";

        prerr_endline "-- vartc --";
        ModelCheck.print_vartc vartc;
        prerr_endline "";

        let new_tj = 
          (Saturation.propagate 
             hes lts stmap nttc vartc toprop)
        in

        let newtypeset = 
          Set.diff 
            (Set.map fst new_tj) 
            (Map.findDefault Set.empty toprop nttc)
        in

        let ntj = ModelCheck.add_judgments tj toprop new_tj in

        prerr_endline "-- ntj --";
        ModelCheck.print_judgments ntj;
        prerr_endline "---------";

        prerr_endline "-- mem --";
        Hashtbl.iter (fun (a,b,c) d -> prerr_endline (Atype.to_string a);prerr_endline (Term.to_string b);prerr_endline c;prerr_endline "") Saturation.memo;
        prerr_endline "---------";

        prerr_endline "-- newtypeset --";
        prerr_endline (string_of_int (Set.cardinal newtypeset));
        prerr_endline "---------";

        let vartc' = 
          ModelCheck.update_vartc flow2 toprop newtypeset vartc
        in

        let nttc' = 
          Map.modifyDefault 
            Set.empty toprop (Set.union newtypeset) nttc
        in

        let ntoprop =
          if Set.is_empty newtypeset 
          then toprops
          else
            ModelCheck.merge_toprop 
              toprops 
              (Set.elements (Map.find toprop f_dep))
        in

        forloop (k+1) (ntj, nttc', vartc', ntoprop)
      end
  in
  forloop 0 (tj, nttc, vartc, toprop)
