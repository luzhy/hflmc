type t = {
  nocheck_safe : bool;
  nocheck_unsafe : bool;
  noopt_erase : bool;
  noopt_weakproof : bool;
  stats : bool;
  verbose : int;
  pgsolver : string;
  iter : int;
  randomize : bool;
  step : int;
  flow_precision : int;
  conjunction : int
}
